﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace Chat
{
	class ChatController
	{
		private UdpClient _udpClient;
		private readonly IPAddress _multicastAddress;
		private readonly IPEndPoint _endPoint;
		private readonly string UserName;

		public ChatController(string userName)
		{
			UserName = userName;
			_multicastAddress = IPAddress.Parse("228.0.0.228");
			_udpClient = new UdpClient();
			_udpClient.JoinMulticastGroup(_multicastAddress);
			_endPoint = new IPEndPoint(_multicastAddress, 1488);
		}

		public void Send(string msg)
		{
			byte[] msgBuffer = Encoding.UTF8.GetBytes(UserName + "> "+ msg);

			_udpClient.Send(msgBuffer, msgBuffer.Length, _endPoint);
		}

		public void Listen()
		{
			UdpClient listener = new UdpClient();
			listener.ExclusiveAddressUse = false;
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 1488);
			listener.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
			listener.Client.Bind(endPoint);
			listener.JoinMulticastGroup(_multicastAddress);

			Console.WriteLine("chat begin");

			while(true)
			{
				byte[] data = listener.Receive(ref endPoint);
				Console.WriteLine(Encoding.UTF8.GetString(data));
			}
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			ChatController controller = new ChatController(Console.ReadLine());
			new Thread(controller.Listen) { IsBackground = true }.Start();

			while(true)
			{				
				controller.Send(Console.ReadLine());
			}
		}

		void ClearLine(int line)
		{
			Console.MoveBufferArea(0, line, Console.BufferWidth, 1, Console.BufferWidth, line, ' ', Console.ForegroundColor, Console.BackgroundColor);
		}
	}
}
