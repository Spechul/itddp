<?xml version="1.0"?>
	<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="page">
		<html>
		<head><title>
			<xsl:value-of select="@name"/>
		</title></head>
		<body>
			<xsl:apply-templates/>
		</body>
		</html>
	</xsl:template>	

	<xsl:template match="p">
		<p><xsl:apply-templates/></p>
	</xsl:template>
	
	</xsl:stylesheet> 