class Element {
  constructor(text) {
    this.id = count;
    count += 1;
    this.image = undefined;
    this.text = text ? text : 'element';
    this.type = '[element]'
  }

  view() {
    return `<div class="menuitem element" id="${this.id}"><button>${this.type} ${this.text}</button></div>`;
  }
}

class Back extends Element {
  constructor(pid) {
    super('<-');
    this.pid = pid;
    //alert(this.pid);
  }

  view() {
    return `<div class="menuitem element" id="${this.id}"><back pid=${this.pid}/><button>${this.text}</button></div>`;
  }
}

class File extends Element {
  constructor(fileSource) {
    let path = fileSource.split('/');
    super(path[path.length - 1]);
    this.type = '[file]';
  }

  view() {
    switch (regime)
    {
      case 0:
        return super.view();
      case 1:
        return `
        <button class="menuitem withimg" id=${this.id}>
          <figure>
              <img src="png/Stack-3-icon.png" alt="">
              <figcaption>${this.text}</figcaption>
          </figure>
        </button>
        `;
    }
    
  }
}

class Img extends File {
  constructor(imgSource) {
    let path = imgSource.split('/')
    //alert(path[path.length - 1]);
    super(path[path.length - 1]);
    this.imgSource = imgSource;
    this.type = '[image]';
  }

  view() {
    switch(regime)
    {
      case 0:
        return super.view();
      case 1:
      return `
      <button class="menuitem withimg" id=${this.id}>
        <figure>
            <img src="${this.imgSource}" alt="">
            <figcaption>${this.text}</figcaption>
        </figure>
      </button>
      `;
    }
    
  }
}

class Folder extends Element {
  constructor(folderSource, pid) {
    let path = folderSource.split('/');
    super(path[path.length - 1]);
    this.pid = pid;
    this.setElements();
    folders.push(this);
    this.type = '[folder]';
  }

  setElements() {
    this.elements = [new Back(this.pid)];
  }

  addElement(element) {
    this.elements.push(element);
    element.pid = this.id;
  }

  view() {
    switch(regime)
    {
      case 0:
        return super.view();
      case 1:
        return `
        <button class="menuitem withimg" id=${this.id}>
          <figure>
              <img src="png/Folder-icon.png" alt="">
              <figcaption>${this.text}</figcaption>
          </figure>
        </button>
        `;
    }
  }
}

class Root extends Folder {
  constructor() {
    super('\\');
  }
  setElements() {
    this.elements = [];
  }
}


function findFolder(id) {
  let folder = root;
  folders.forEach(element => {
    if (element.id === id)
      folder = element;
  });
  return folder;
}



function formInnerHTML(elems) {
  str = '';
  elems.forEach(element => {
    str += element.view();
  });
  return str;
}

function replaceInnerHTML(id, elems) {
  document.getElementById(id).innerHTML = formInnerHTML(elems);

  elems.forEach(element => {
    document.getElementById(element.id).addEventListener('click', function () {
      if (element instanceof Folder) {
        currentFolder = element;
        replaceInnerHTML(id, element.elements);
      }
      else if (element instanceof Back) {
        currentFolder = findFolder(element.pid);
        replaceInnerHTML(id, currentFolder.elements);
      }
      else if (element instanceof Img) {
        window.open(element.imgSource);
      }
    })
  });

  replaceFolders(currentFolder.elements);
}

function switchRegime() {
  if (regime > 0) {
    regime = 0;
    return;
  }
  regime += 1;
}

function switchOverlay() {
  switch(regime) {
    case 0:
    document.getElementById('holder').setAttribute('class', 'folderview');
      break;

    case 1:
    document.getElementById('holder').setAttribute('class', 'folderview-flex');
      break;

    default:
      throw new Error('only 2 regimes allowed');
  }
}

function replaceFolders(folders) {
  var list = document.getElementById('folders');
  list.innerHTML = ''
  currentFolder.elements.forEach(element => {
    if (element instanceof Folder)//element.type == '[folder]')
      list.innerHTML += `<option value="">${element.text}</option>`;
  });
  
}

let regime = 0;
let count = 0;

folders = [];
let f = new Folder('asd');
let f1 = new Folder('qwe');
let f2 = new Folder('zxc', f1.id);
f2.addElement(new Img('image/552.jpg'));
f.addElement(new Img('image/whitera.jpg'));
f1.addElement(f2);
f1.addElement(new File('test.txt'));
f1.addElement(new File('test2.txt'));

currentFolder = root = new Root();
currentFolder.addElement(f);
currentFolder.addElement(f1);
currentFolder.addElement(new File('some document'));
currentFolder.addElement(new Img('image/aaa.jpg'));


document.getElementById('rt').addEventListener('click', function () {
  replaceInnerHTML('holder', root.elements);
  
});

document.getElementById('switch').addEventListener('click', function () {
  switchRegime();
  //alert(`regime is ${regime}`);
  switchOverlay()
  replaceInnerHTML('holder', currentFolder.elements)
  //replaceFolders(currentFolder.elements);
})
